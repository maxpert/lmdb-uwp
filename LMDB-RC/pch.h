﻿#pragma once

#include <collection.h>
#include <ppltasks.h>

#ifdef _WINRT_DLL
#ifndef _WIN32
#define _WIN32
#endif
#define WIN_RT
#endif

#include <windows.h>
#include <stdlib.h>

#ifndef WINBASEAPI
#define WINBASEAPI __declspec(dllimport)
#endif

#ifndef DLL_API
#define DLL_API __declspec(dllexport)
#endif

#ifdef WIN_RT
WINBASEAPI BOOL WINAPI FlushViewOfFile(LPCVOID, SIZE_T);
WINBASEAPI BOOL WINAPI UnmapViewOfFile(LPCVOID);
#endif
